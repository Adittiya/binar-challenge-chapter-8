import React, { useState } from "react";

function CreatePlayer() {
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [inputText, setInputText] = useState({ Username: "", Email: "" });
  const [submitError, setSubmitError] = useState("");

  function handleUserName(event) {
    console.log(event.target.value);
    setUserName(event.target.value);
  }

  function handleEmail(event) {
    console.log(event.target.value);
    setEmail(event.target.value);
  }
  function handlePassword(event) {
    console.log(event.target.value);
    setPassword(event.target.value);
  }

  function handleSubmit(event) {
    event.preventDefault();

    if (
      userName.trim() === "" ||
      email.trim() === "" ||
      password.trim() === ""
    ) {
      setSubmitError("Please fill out all fields.");

      setTimeout(() => {
        setSubmitError("");
      }, 5000);

      return;
    }

    setInputText({ Username: userName, Email: email });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="container">
        <h1>Create Player</h1>
        <p>{submitError}</p>

        <input
          onChange={handleUserName}
          name="userName"
          placeholder="Username"
          value={userName}
        />
        <input
          onChange={handleEmail}
          name="email"
          type="email"
          placeholder="Email"
          value={email}
        />
        <input
          onChange={handlePassword}
          name="password"
          type="password"
          placeholder="Password"
          value={password}
        />
        <button className="Submit">Create</button>

        {inputText.Username && <p>Your Username is: {inputText.Username}</p>}
        {inputText.Email && <p>Your Email is: {inputText.Email}</p>}
      </div>
    </form>
  );
}

export default CreatePlayer;
