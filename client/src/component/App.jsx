import React, { useState } from "react";
import Header from "./Header";
import CreatePlayer from "./CreatePlayer";
import WaveAnimation from "./WaveAnimation";
import EditPlayer from "./EditPlayer";
import FindPlayer from "./FindPlayer";

function App() {
  const [activeComponent, setActiveComponent] = useState("create");

  const handleHeaderClick = (component) => {
    setActiveComponent(component);
  };

  let componentToRender;
  if (activeComponent === "create") {
    componentToRender = <CreatePlayer />;
  } else if (activeComponent === "edit") {
    componentToRender = <EditPlayer />;
  } else if (activeComponent === "find") {
    componentToRender = <FindPlayer />;
  }

  return (
    <>
      <Header onHeaderClick={handleHeaderClick} />
      {componentToRender}
      <WaveAnimation />
    </>
  );
}

export default App;
