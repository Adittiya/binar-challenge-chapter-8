import React from "react";

function Header(props) {
  const handleLinkClick = (component) => {
    props.onHeaderClick(component);
  };

  return (
    <header>
      <h1>Form</h1>
      <nav>
        <ul className="header-links">
          <li>
            <button
              type="button"
              className="header-link"
              onClick={() => handleLinkClick("create")}
            >
              Create Player
            </button>
          </li>
          <li>
            <button
              type="button"
              className="header-link"
              onClick={() => handleLinkClick("edit")}
            >
              Edit Player
            </button>
          </li>
          <li>
            <button
              type="button"
              className="header-link"
              onClick={() => handleLinkClick("find")}
            >
              Find Player
            </button>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
