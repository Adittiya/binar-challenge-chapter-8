import React, { useState } from "react";

function EditPlayer() {
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [level, setLevel] = useState("");
  const [password, setPassword] = useState("");
  const [inputText, setInputText] = useState({ Username: "", Email: "" });
  const [submitError, setSubmitError] = useState("");

  function handleUserName(event) {
    console.log(event.target.value);
    setUserName(event.target.value);
  }

  function handleEmail(event) {
    console.log(event.target.value);
    setEmail(event.target.value);
  }
  function handleLevel(event) {
    console.log(event.target.value);
    setLevel(event.target.value);
  }

  function handlePassword(event) {
    console.log(event.target.value);
    setPassword(event.target.value);
  }

  function handleSubmit(event) {
    event.preventDefault();

    if (
      userName.trim() === "" ||
      email.trim() === "" ||
      level.trim() === "" ||
      password.trim() === ""
    ) {
      setSubmitError("Please fill out all fields.");

      setTimeout(() => {
        setSubmitError("");
      }, 5000);

      return;
    }

    setInputText({ Username: userName, Email: email, Level: level });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="container">
        <h1>Edit Player</h1>
        <p>{submitError}</p>

        <input
          onChange={handleUserName}
          name="userName"
          placeholder="Username"
          value={userName}
        />
        <input
          onChange={handleEmail}
          name="email"
          type="email"
          placeholder="Email"
          value={email}
        />
        <input
          onChange={handleLevel}
          name="Level"
          placeholder="Level"
          value={level}
        />
        <input
          onChange={handlePassword}
          name="password"
          type="password"
          placeholder="Password"
          value={password}
        />
        <button className="Submit">Edit</button>

        {inputText.Username && <p>Your Username is: {inputText.Username}</p>}
        {inputText.Email && <p>Your Email is: {inputText.Email}</p>}
        {inputText.Level && <p>Your Level is: {inputText.Level}</p>}
      </div>
    </form>
  );
}

export default EditPlayer;
