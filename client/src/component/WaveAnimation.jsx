import React from "react";
import Wave from "react-wavify";

function WaveAnimation() {
  return (
    <div
      style={{
        position: "fixed",
        bottom: 0,
        width: "100%",
        height: "10vh",
        zIndex: -1,
      }}
    >
      <Wave
        fill="#ffffff"
        paused={false}
        options={{
          height: 20,
          amplitude: 40,
          speed: 0.2,
          points: 4,
        }}
        style={{
          height: "100%",
          width: "100%",
        }}
      />
    </div>
  );
}

export default WaveAnimation;
